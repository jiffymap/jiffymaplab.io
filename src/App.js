import React from "react";
import { Map as LeafletMap, GeoJSON, TileLayer } from "react-leaflet";
import queryString from "query-string";
import yaml from "yamljs";
import pako from "pako";

import "./App.css";
const position = [45.777222, 3.087025];

const replaceAll = (str, find, replace) => {
  return str.replace(new RegExp(find, "g"), replace);
};

const onEachFeature = (feature, layer) => {
  if (feature.properties) {
    layer.bindPopup(yaml.stringify(feature.properties, 4));
  }
};

const JiffyMap = () => {
  const params = queryString.parse(window.location.search);
  let data = null;

  if ("geojson" in params) {
    let temp = params.geojson;
    temp = replaceAll(temp, "%5D", "]");
    temp = replaceAll(temp, "%7D", "}");
    data = JSON.parse(temp);
  }

  if ("geojson_compressed" in params) {
    let temp = params.geojson_compressed;
    temp = atob(temp);
    temp = pako.inflate(temp, { to: "string" });
    data = JSON.parse(JSON.parse(temp));
  }

  return (
    <LeafletMap center={position} zoom={6}>
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.osm.org/{z}/{x}/{y}.png"
      />
      {data && <GeoJSON data={data} onEachFeature={onEachFeature} />}
    </LeafletMap>
  );
};

export default JiffyMap;
